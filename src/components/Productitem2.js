import React from "react";

class Productitem2 extends React.Component {

    itemTwo = React.createRef();

    handleClick = event => {
        {/* This handles the event of anything within the input changing, and logs the new value*/}
        event.preventDefault();

        {/* Store the new value in a variable */} 
        let itemTwoValue = this.itemTwo.current.value;

        {/* Display the new value just for testing purposes */}
        console.log(itemTwoValue);

        this.props.addValueTwo();

        {/* This calculates the value between the value of input and the price of the item
            and the total is then ready to be sent back to state for updating and displaying
            in the total price section
         */}
        let totalTwo = (itemTwoValue * 2.50);
        
        console.log(totalTwo);

    }

    render() {
      return (
        <tr className="productitm">
            <td>Desperados</td>
            <td>
                <input type="number" className="qtyinput" defaultValue= {1} ref={this.itemTwo} onChange={this.handleClick.bind(this)} />
            </td>
            <td>£2.50</td>
            <td></td>

            {/* Picture of rubbish bin added for interest. Clicking this will clear the product quantity and take it out of the customers bag */}
            <td><span className="remove"><img src="https://i.imgur.com/h1ldGRr.png" alt="X" /></span></td>

        </tr>
      )
    }
  }

export default Productitem2;