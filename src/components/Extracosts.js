import React from "react";

{/* Extra costs will be the VAT and necessary shipping charge with every purchase

The logic would be..

Total Price of Item One * 0.20

Total Price of Item Two * 0.20

Total Price of Item Three * 0.20

and then shipping would be a flat rate of £5, however I would look to implement a shipping
weight cost to add more per weight of the item decalred in the products object.

*/}

class Extracosts extends React.Component {
    render() {
        return(
            <tr className="extracosts">
                <td className="light">Shipping &amp; Tax</td>
                <td colSpan="2" className="light"></td>
                <td>£5.00</td>
                <td>&nbsp;</td>
          </tr>
        )
    }
}

export default Extracosts;