{/* List of products to reference later on */}

export const productList = [
            { name: "Mountain Dew", price: 130, info: "For long hours of dev work" },
            { name: "Desperados", price: 345, info: "Because who doesn't love Tequila?" },
            { name: "Jack Daniels", price: 768, info: "A slightly harder drink" }
          ];