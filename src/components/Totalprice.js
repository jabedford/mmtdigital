import React from "react";
import { TotalPrice } from "./ProductItem";

{/* This component will take the total price from the item components and display it */}

class Totalprice extends React.Component {
    render() {
        return(
            <tr className="totalprice">
                <td className="light">Total:</td>
                <td colSpan="2">&nbsp;</td>
                <td colSpan="2"><span className="thick">£0</span></td>
          </tr>
        )
    }
}

export default Totalprice;