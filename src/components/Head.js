import React from "react";

/* This component is the top of the table and provides the headers for the columns */

class Head extends React.Component {
    render() {
        return(
            <thead>
                <tr>
                    <th className="first">Product</th>
                    <th className="second">Qty</th>
                    <th className="third">Price</th>
                    <th className="fourth"></th>
                </tr>
            </thead>
        )
    }
}

export default Head;