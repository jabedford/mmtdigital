import '@assets/favicon.ico'
import '@styles/styles.scss'

/**
 * Welcome! This project has been setup with webpack to — hopefully —
 * just work. You should be able to write JS here and it will be compiled
 * into a file called "bundle.js", which is referenced in the 
 * `public/index.html` file.
 * 
 * You can either dump all your JS here, or organise it using import/export
 * as this build should fully support ES Modules.
 */

/**
 * Here's an example of importing an image (so webpack processes it)
 * This (as it's an image) will end up in the assets/images directory.
 */
import trump from '@images/trump.jpg'


/*
 * Want to use React? It's installed and ready to go. Just uncomment
 * the code below to get started...
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Head from './components/Head';
import Productitem from './components/Productitem';
import Productitem2 from './components/Productitem2';
import Productitem3 from './components/Productitem3';
import Extracosts from './components/Extracosts';
import Totalprice from './components/Totalprice';
import Checkout from './components/Checkout';
import { updateQuantity } from './components/Productitem';

class App extends React.Component {
  state = {
    itemOne: {},
    itemTwo: {},
    itemThree: {},
    basketTotal:{}
  }

  addValue = (itemValue) => {
    const itemOne = {...this.state.itemOne};
  }

  addValueTwo = (itemValueTwo) => {
    const itemOne = {...this.state.itemOne};
  }

  addValueThree = (itemValueThree) => {
    const itemOne = {...this.state.itemOne};
  }

  render() {
      return(
          <div id="w">
              <div id="page">
                <form className="basket" >
                  <table id="cart">
                  <Head />
                        <tbody>
                          <Productitem addValue={this.addValue}/>
                          <Productitem2 addValueTwo={this.addValueTwo}/>
                          <Productitem3 addValueThree={this.addValueThree}/>
                          <Extracosts />
                          <Totalprice />
                          <Checkout />
                        </tbody>
                  </table>
                </form>
              </div>
          </div>
      )
  }
}

 ReactDOM.render(<App />, document.getElementById('root'))



