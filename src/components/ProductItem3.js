import React from "react";

class Productitem3 extends React.Component {

    itemThree = React.createRef();

    handleClick = event => {
        {/* This handles the event of anything within the input changing, and logs the new value*/}
        event.preventDefault();

        {/* Store the new value in a variable */} 
        let itemThreeValue = this.itemThree.current.value;

        {/* Display the new value just for testing purposes */}
        console.log(itemThreeValue);

        this.props.addValueThree();

        {/* This calculates the value between the value of input and the price of the item
            and the total is then ready to be sent back to state for updating and displaying
            in the total price section
         */}
        let totalThree = (itemThreeValue * 7.50);

        console.log(totalThree);
    }

    render() {
      return (
        <tr className="productitm">
            <td>Jack Daniels</td>
            <td>
                <input type="number" className="qtyinput" defaultValue= {1} ref={this.itemThree} onChange={this.handleClick.bind(this)} />
            </td>
            <td>£7.50</td>
            <td></td>

            {/* Picture of rubbish bin added for interest. Clicking this will clear the product quantity and take it out of the customers bag */}
            <td><span className="remove"><img src="https://i.imgur.com/h1ldGRr.png" alt="X" /></span></td>

        </tr>
      )
    }
  }

export default Productitem3;