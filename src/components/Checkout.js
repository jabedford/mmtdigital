import React from "react";

class Checkout extends React.Component {
    render() {
        return(
            <tr className="checkoutrow">
                <td colSpan="5" className="checkout">
                    {/*  The clear button just relies simply on refreshing the page and therefore resetting */}
                    <a href="/">Clear</a>

                    {/* The checkout button will take the customer off to the final checkout and rely on React Router */}
                    <button id="submitbtn">Checkout</button>
                </td>
            </tr>
        )
    }
}

export default Checkout;