import React from "react";

class Productitem extends React.Component {

    item = React.createRef();

    handleClick = event => {
        {/* This handles the event of anything within the input changing, and logs the new value*/}
        event.preventDefault();

        {/* Store the new value in a variable */} 
        let itemValue = this.item.current.value;

        {/* Display the new value just for testing purposes */}
        console.log(itemValue);

        this.props.addValue();

        {/* This calculates the value between the value of input and the price of the item
            and the total is then ready to be sent back to state for updating and displaying
            in the total price section
         */}
        let totalOne = (itemValue * 1.99);
        
        console.log(totalOne);
    }

    render() {
      return (
        <tr className="productitm">
            <td>Mountain Dew</td>
            <td>
                <input type="number" className="qtyinput" defaultValue= {1} ref={this.item} onChange={this.handleClick.bind(this)} />
            </td>
            <td>£1.99</td>
            <td></td>

            {/* Picture of rubbish bin added for interest. Clicking this will clear the product quantity and take it out of the customers bag */}
            <td><span className="remove"><img src="https://i.imgur.com/h1ldGRr.png" alt="X" /></span></td>

        </tr>
      )
    }
  }

export default Productitem;